﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Threading;     //for timer on threads
using System.Diagnostics;   //for getting process list
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;   //importing user32.dll
using System.Drawing.Imaging;
using System.ComponentModel; //for background worker
using System.Runtime.Serialization.Formatters.Binary; //for serializing images into byte arrays to then convert to hash
using System.IO; //for memstream
using System.Security.Cryptography; //for hashing the image into SHA256


namespace EbookToBmp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private ProcessedProcessData _processData;  //process data that the program revolves around. should have all the relevant processes and window handles stored in here.
        private WindowControl _windowCtrl;
        private EBookData bookData;
        private WinScreenshot _screenshot;
        //for checking on processes
        private BackgroundWorker _bgProcessMonitor;     //thread for monitoring processes
        private BackgroundWorker _bgSendToEReader;      //thread for sending keypresses to window
        private System.Threading.Timer _timer;
        private bool _capturing;
        
        //housekeeping stuff to apply to files, controls, etc...
        private int KEY = 0;
        //Getting root directory
        private FolderBrowserDialog folderBrowserDialog;

        const string EREADER_NAME = "eReader name: ";
        string ename = "";

        public MainWindow() {
            InitializeComponent();
            _processData = new ProcessedProcessData();
            _windowCtrl = new WindowControl();
            bookData = new EBookData();
            _screenshot = new WinScreenshot();
            //actually create threads
            _bgProcessMonitor = new System.ComponentModel.BackgroundWorker();
            _bgSendToEReader = new System.ComponentModel.BackgroundWorker();
            _capturing = false;
            //initialize thread actions/properties/etc
            InitializeBackgroundWorkerProcessMonitor();
            InitializeBackgroundWorkerWindowCtrl();

            procList.DisplayMemberPath = "Name";    //tell the  listbox to "get" display the names. We keep the handles for later.
            procList.ItemsSource = this._processData.Processes;
            this.folderBrowserDialog = new FolderBrowserDialog {
                Description = "Select the directory that you want to use as the root library folder."
            };
            this.UserDir.IsEnabled = false;
        }
        /// <summary>
        /// Grabs a list of processes with windows with names. Basically stuff that the user SHOULD see. It then adds the list of processes with user readable names to the listbox for selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetProccesses_Click(object sender, RoutedEventArgs e) {
            if(_processData.CheckingProcesses == false) {
                this._processData.CheckingProcesses = true;
                this.getProccesses.Content = "Stop listing";
                this._bgProcessMonitor.RunWorkerAsync();
            }
            else {
                this._processData.CheckingProcesses = false;
                this.getProccesses.Content = "List Processes";
                int index = this.procList.Items.IndexOf(this._processData.Temp.Handle);
                this._bgProcessMonitor.CancelAsync();
            }
        }


        /// <summary>
        /// This button is for testing the logic. Use it for whatever.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Debug_Click(object sender, RoutedEventArgs e) {
            //create new thread to run in the background
            _windowCtrl.SendToEReader(this._processData.Temp, KEY);
        }
        /// <summary>
        /// Shows that the process is selected and loads the relevant process information (handle and name) into a temp object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProcList_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            //get selected item
            this._processData.Temp = (ListedProcess)procList.SelectedItem;
            //if there's a selected item with an actual name then set it to Temp
            if (this._processData.Temp != null && this._processData.Temp.Name != null) {
                ename = this._processData.Temp.Name; //put the name in its proper place
                this._processData.OldTemp = new ListedProcess(this._processData.Temp.Name, this._processData.Temp.Handle); //save it in old temp for checking if it still exists when processes change
            }
            //check if the old temp is still in the process list and reselect it
            else
            {
                //return index of 0 or greater if the process still exists
                int index = this._processData.OldTempProcessStillExists();
                procList.SelectedItem = index;
                if (index > 0) {
                    ename = this._processData.OldTemp.Name;
                    this._processData.Temp = this._processData.OldTemp;
                }
                else
                    ename = "null";
            }
            eReader.Text = EREADER_NAME + ename;
        }

        #region //BackgroundWorker stuff for the process monitor
        private void InitializeBackgroundWorkerProcessMonitor() {
            //allow it to be cancelled
            _bgProcessMonitor.WorkerSupportsCancellation = true;
            //this is what the background worker works on when it gets the signal
            _bgProcessMonitor.DoWork += new DoWorkEventHandler(BgProcessMonitor_DoWork);
            //this is what the background worker does when it is done
            _bgProcessMonitor.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BgProcessMonitor_Completed);
        }

        /// <summary>
        /// Calls function to be looped on a separate thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BgProcessMonitor_DoWork(object sender, DoWorkEventArgs e) {
            //Get background worker that raised the event
            BackgroundWorker worker = sender as BackgroundWorker;

            //keep checking processes until user says to stop checking processes
            while(this._processData.CheckingProcesses == true) {
                this._processData.MonitorProcesses(worker, e); }
            return;
        }
        /// <summary>
        /// Messages to show when done monitoring processes. It should only really show errors or if it was cancelled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BgProcessMonitor_Completed(object sender, RunWorkerCompletedEventArgs e) {
            if (e.Error != null)
                System.Windows.MessageBox.Show(e.Error.Message);
            else if (e.Cancelled)
                System.Windows.MessageBox.Show("Operation was cancelled");
        }
        #endregion

        #region //BackgroundWorker stuff for the image capture
        private void InitializeBackgroundWorkerWindowCtrl() {
            //allow thread work to be cancelled
            this._bgSendToEReader.WorkerSupportsCancellation = true;
            //do the keypress stuff
            this._bgSendToEReader.DoWork += new DoWorkEventHandler(BgSendToEReader_DoWork);
            //what to do when done
            this._bgSendToEReader.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BgSendToEReader_Completed);
        }
        /// <summary>
        /// Returns true if OK for capturing next page. Returns false if not OK
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private void BgSendToEReader_DoWork(object sender, DoWorkEventArgs e) {

            this._timer = new System.Threading.Timer(CaptureAndKeypress, null, 1500, Timeout.Infinite);
        }

        private void CaptureAndKeypress(object state) {
            if (this._bgSendToEReader.CancellationPending == true)
                return;
            else {
                try {
                    //update page number
                    App.Current.Dispatcher.Invoke((Action)delegate
                    {
                        this.ManualPageNumber.Text = this.bookData.Page;
                    });
                    //take screenshot - save and send next page key if page is new
                    //capture window to temp image memory
                    this._screenshot.TempImage = this._screenshot.CaptureWindow(this._processData.Temp.Handle);
                    //check if it is the same image as last time

                    //if statement is true if the image is the same
                    if (this._screenshot.CheckSameImage()) {
                        //say that a the image is the same as the last one
                        System.Windows.MessageBox.Show("Duplicate image detected");
                    }
                    else {
                        //else save to file and send next key if it is not the same image
                        this._screenshot.SaveToFile(this.bookData.FullDirectoryPath, this._screenshot.ImgFormat);
                        this._windowCtrl.SendToEReader(this._processData.Temp, this.KEY);
                        this.bookData.IncrementPage();
                        this._timer.Change(1500, Timeout.Infinite);

                    }
                }
                catch (Exception ohno) {
                    System.Windows.MessageBox.Show(ohno.ToString() + "\n\nException caught.");
                }
            }
        }

        private void BgSendToEReader_Completed(object sender, RunWorkerCompletedEventArgs e) {
            if (e.Error != null)
                System.Windows.MessageBox.Show(e.Error.Message);
            else if (e.Cancelled)
                System.Windows.MessageBox.Show("Operation was cancelled");
        }
        #endregion

        private void setFG_Click(object sender, RoutedEventArgs e) {
            _windowCtrl.ShowToUser(this._processData.Temp);
        }


        #region //buttons for setting book details
        /// <summary>
        /// Folder browser dialogue for getting the root folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Directory_Click(object sender, RoutedEventArgs e) {
            //show the dialogue and update the textbox that shows selected path
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                //update the textbox and update the book details
                this.UserDir.Text = folderBrowserDialog.SelectedPath;
                this.bookData.UserDirectory = folderBrowserDialog.SelectedPath;
            }
        }
        /// <summary>
        /// Update title and author strings when user specifies them
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetTitleAuthor_Click(object sender, RoutedEventArgs e) {
            //forward the data to the book object to be stored
            this.bookData.Title = this.TitleBox.Text;
            this.bookData.Author = this.AuthorBox.Text;
        }
        /// <summary>
        /// Maybe you interrupted the process and want to start where you left off. That's why you can set your own page number.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pgNum_Click(object sender, RoutedEventArgs e) {
            this.bookData.Page = this.ManualPageNumber.Text;
            //update page number in UI to default if garbage was entered.
            this.ManualPageNumber.Text = this.bookData.Page;
        }
        #endregion

        //screencap test button
        private void Screencap_Click(object sender, RoutedEventArgs e) {
            if (_capturing == false) {
                this._bgSendToEReader.RunWorkerAsync();
                _capturing = true;
                this.Screencap.Content = "Stop Screenshots";
            }
            else {
                this._bgProcessMonitor.CancelAsync();
                _capturing = false;
                this.Screencap.Content = "Capture Screenshots";
            }
        }

        #region //radio button logic for key presses
        private void Right_Arrow_Checked(object sender, RoutedEventArgs e) {
            this.KEY = 0;
        }

        private void PG_DN_Checked(object sender, RoutedEventArgs e) {
            this.KEY = 1;
        }
        #endregion

        #region //ImageFormat buttons
        private void PngFormat_Checked(object sender, RoutedEventArgs e)
        {
            this._screenshot.ImgFormat = ImageFormat.Png;
        }
        private void JpgFormat_Checked(object sender, RoutedEventArgs e)
        {
            this._screenshot.ImgFormat = ImageFormat.Jpeg;
        }

        private void BmpFormat_Checked(object sender, RoutedEventArgs e)
        {
            this._screenshot.ImgFormat = ImageFormat.Bmp;
            System.Windows.MessageBox.Show("Yeah. You clicked this.");
        }
        #endregion
    }


    #region //a class for holding process information
    public class ListedProcess
    {
        private readonly string _name;
        private readonly IntPtr _handle;

        public ListedProcess(string name, IntPtr handle)
        {
            this._name = name;
            this._handle = handle;
        }

        public string Name
        {
            get { return _name; }
        }

        public IntPtr Handle
        {
            get { return _handle; }
        }
    }
    #endregion

    #region //a class for holding all the processed process data
    public class ProcessedProcessData {
        private Process[] localAll; //for listing all the currently running processes
        private ListedProcess temp; //for the temp process  when the user selects it
        private ListedProcess oldTemp;
        private ObservableCollection<ListedProcess> processes;//for filtered processes that have names/windows
        private List<ListedProcess> sortedProcesses; //for sorted processes
        private List<ListedProcess> oldProcesses; //old processes for comparing. Sometimes you just don't need to update.
        bool checkingProcesses; 

        //constructor to set things up the way they need to be.
        public ProcessedProcessData () {
            this.processes = new ObservableCollection<ListedProcess>();
            this.oldProcesses = new List<ListedProcess>();
            this.checkingProcesses = false;
            this.sortedProcesses = new List<ListedProcess>();
        }
       
        /// <summary>
        /// This sorts the list of processes.
        /// </summary>
        /// <param name="sortTheseProcesses"></param>
        private void SortProcesses(ref List<ListedProcess> sortTheseProcesses) {
            sortedProcesses.Sort(delegate (ListedProcess lp1, ListedProcess lp2) { return lp1.Name.CompareTo(lp2.Name); });
        }

        /// <summary>
        /// This function filters out processes that do not have windows and returns a list that only contains processes with windows
        /// </summary>
        /// <param name="allProcesses"></param>
        /// <returns></returns>
        private List<ListedProcess> FilterProcesses(Process[] allProcesses) {
            //make a temp list to put all the processes inside
            List<ListedProcess> temp = new List<ListedProcess>();
            //for each processes in the process array, only add the processes that have a window title and the main window handle
            foreach(Process proc in allProcesses) {
                if(proc.MainWindowTitle.Length > 0) {
                    temp.Add(new ListedProcess(proc.MainWindowTitle, proc.MainWindowHandle));
                }
            }

            return temp;
        }

        //use these processes to display to to the user
        private void UpdateDisplayedProcesses() {
            //Clear the list
            App.Current.Dispatcher.Invoke((Action)delegate { this.processes.Clear(); });

            //Add new processes into the observable list
            foreach (ListedProcess proc in this.sortedProcesses) {
                App.Current.Dispatcher.Invoke((Action)delegate {
                    this.processes.Add(new ListedProcess(proc.Name, proc.Handle));
                });

            }
        }

        //function for comparing old process list to new process list
        private bool CompareProcesses(List<ListedProcess> oldProcesses, List<ListedProcess> newProcesses)
        {
            //if the counts are different then clearly the list has changed
            if (oldProcesses.Count() != newProcesses.Count())
                return true;
            //If the handles changed then return true
            for (int i = 0; i < oldProcesses.Count(); i++) {
                if (oldProcesses[i].Handle != newProcesses[i].Handle)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Returns -1 if the process doesn't exist in the new sortedProcess.
        /// Returns the index of the process if it still exists in the new sortedProcess
        /// </summary>
        /// <returns></returns>
        public int OldTempProcessStillExists() {
            if (this.oldTemp != null)
            {//go through the list of processes and see if it is in there
                for (int i = 0; i < sortedProcesses.Count(); i++) {
                    //if the window and handle still exist then return the index
                    if (sortedProcesses[i].Handle == this.oldTemp.Handle) {
                        this.Temp = this.oldTemp; //set temp back to oldTemp
                        return i;
                    }
                }
                return -1; //return -1 if not in the list
            }
            return -1; //return -1 if oldTemp is null
        }

        //keeps getting the list of processes and updating it as needed
        public void MonitorProcesses(BackgroundWorker worker, DoWorkEventArgs e) {
            //check if user canceled the task.
            if (worker.CancellationPending)
                e.Cancel = true;
            
            //otherwise do the task
            else {
                //get the list of all processes on the system
                localAll = Process.GetProcesses();
                //filter out processes without windows into sortedProcesses
                sortedProcesses = FilterProcesses(localAll);
                //sort the processes alphabetically
                SortProcesses(ref sortedProcesses);
                //check if old and new sorted processes match
                if (CompareProcesses(oldProcesses, sortedProcesses)) {
                    //clear old processes
                    oldProcesses.Clear();
                    //copy sorted processes into old processes
                    oldProcesses = sortedProcesses.ConvertAll(proc => new ListedProcess(proc.Name, proc.Handle));
                    UpdateDisplayedProcesses();
                }
            }
        }

        //returns processes for listbox
        public ObservableCollection<ListedProcess> Processes {
            get { return processes; }
        }
        public ListedProcess Temp {
            set { temp = value; }
            get { return temp; }
        }
        public ListedProcess OldTemp {
            get { return this.oldTemp; }
            set { this.oldTemp = value; }
        }
        public bool CheckingProcesses {
            get { return checkingProcesses; }
            set { checkingProcesses = value; }
        }
    }
    #endregion

    #region //a class for controlling the windows of processes
    /// <summary>
    /// This controls the windows that is selected from the list of processes.
    /// </summary>
    public class WindowControl {
        //options for radio buttons
        private readonly string[] KEYOPTIONS = { "{RIGHT}", "{PGDN}" };
        private const int SW_SHOWMAXIMIZED = 3;

        //bring window to foreground
        [DllImport("User32.dll")]
        private static extern IntPtr SetForegroundWindow(IntPtr hwnd);

        //maximize the window
        [DllImport("User32.dll")]
        private static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

        //This is a constructor. It doesn't really do anything atm, but it should exist so I can go back and change things if need be.
        public WindowControl() {

        }

        //brings the temp process to the foreground
        private void SetForeground(ListedProcess temp) {
            if(!CheckTempNull(temp)) {
                SetForegroundWindow(temp.Handle);
            }
        }
        //maxmizes the process that is hopefully the ereader process
        private void SetMaximized(ListedProcess temp) {
            if(!CheckTempNull(temp)) {
                ShowWindow(temp.Handle, SW_SHOWMAXIMIZED);
            }
        }
        /// <summary>
        /// Checks if the temp process is null or not. 
        /// 
        /// Returns true if temp is null.
        /// Returns false if temp is not null.
        /// </summary>
        /// <param name="temp"></param>
        /// <returns></returns>
        private bool CheckTempNull(ListedProcess temp) {
            if(temp == null || temp.Name == null || temp.Handle == null) {
                System.Windows.MessageBox.Show("Whoops! The selected thread is either null or has a null handle. Try selecting it again!\nThe process may have closed and you need to open it again.");
                return true;
            }
            return false;
        }


        /// <summary>
        /// Sends the key to selected process window.
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="KEY"></param>
        public void SendToEReader(ListedProcess temp, int KEY) {
            SetForeground(temp);
            SetMaximized(temp);
            SendKeys.SendWait(this.KEYOPTIONS[KEY]);
        }
        /// <summary>
        /// Brings the selected process window up for the user to see that the window they want to be shown is selected and accessible.
        /// </summary>
        /// <param name="temp"></param>
        public void ShowToUser(ListedProcess temp) {
            SetForeground(temp);
            SetMaximized(temp);
        }
    }
    #endregion

    #region //a class for taking screenshots of the selected process' window
    /// <summary>
    /// Takes screenshot of selected window and saves it to a file.
    /// </summary>
    public class WinScreenshot {
        private byte[] _newImgByteArray; //use this for hashing for quicker comparison
        private byte[] _oldHash;
        private byte[] _newHash;
        private bool _DuplicateDetected;
        private ImageFormat _imgFormat;
        private System.Drawing.Image _tempImage;

        private System.Drawing.Image _screenshot;

        public WinScreenshot() {
            //these should all be null. they'll get filled out when stuff gets hashed
            this._newImgByteArray = null;
            this._oldHash = null;
            this._newHash = null;
            this._screenshot = null;
            this._tempImage = null;
            this._DuplicateDetected = false;
            //default to BMP
            this._imgFormat = ImageFormat.Bmp;
        }

        public ImageFormat ImgFormat {
            get { return this._imgFormat; }
            set { this._imgFormat = value; }
        }

        public System.Drawing.Image TempImage {
            get { return this._tempImage; }
            set { this._tempImage = value; }
        }


        /// <summary>
        /// Returns the sha256 hash of the image byte array
        /// </summary>
        /// <param name="imgByteArray"></param>
        /// <returns></returns>
        public static byte[] HashImageByteArray(byte[] imgByteArray) {
            //create sha256 hasher
            SHA256 mySHA256 = SHA256Managed.Create();
            return mySHA256.ComputeHash(imgByteArray);
        }

        /// <summary>
        /// Returns a .NET image object from the selected window. Utilizes some calls to C++ libraries iirc. These are contained in private classes.
        /// </summary>
        /// <param name="winHandle"></param>
        /// <returns></returns>
        public System.Drawing.Image CaptureWindow(IntPtr winHandle) {
            IntPtr hDC = User32.GetWindowDC(winHandle);    //Get device contect for target window
            RECT winRect = new RECT();              //make rect object for holding target window rect info
            User32.GetWindowRect(winHandle, ref winRect);  //save the rect info from target window

            //so rects are doubles for some reason, but we can make this work
            int width = (int)(winRect.Right - winRect.Left);
            int height = (int)(winRect.Bottom - winRect.Top);

            //create device context
            IntPtr hdcDest = GDI32.CreateCompatibleDC(hDC);

            //create a bitmap to copy to
            IntPtr hBitmap = GDI32.CreateCompatibleBitmap(hDC, width, height);

            //load the bitmap made from source device context into the destination device context, returns handle of hdcDest. hdcDest is now a bitmap
            IntPtr hOld = GDI32.SelectObject(hdcDest, hBitmap);

            //transfer bitblock color data from selected window to hdcDest
            GDI32.BitBlt(hdcDest, 0, 0, width, height, hDC, 0, 0, GDI32.SRCCOPY);

            //give hdcDest its handle back
            GDI32.SelectObject(hdcDest, hOld);

            //delete the handle
            GDI32.DeleteDC(hdcDest);

            //delete and release DCs
            GDI32.DeleteDC(hdcDest);
            User32.ReleaseDC(winHandle, hDC);  //we grabbed it when we got the window DC

            //make image object from hBitmap
            System.Drawing.Image img = System.Drawing.Image.FromHbitmap(hBitmap);

            //free the hBitmap object
            GDI32.DeleteObject(hBitmap);

            return img;
        }
        /// <summary>
        /// Tries to save image as a file. Try-Catch structure. Hopefully it works, but it will show a pop-up with whatever error it encounters.
        /// </summary>
        /// <param name="winHandle"></param>
        public void SaveToFile(string filename, ImageFormat format)
        {
            //gotta do this as a try-catch actually. the function is to just capture it as a file. lol
            try {
                //try to save it
                this._tempImage.Save(filename + "." + this.ImgFormat.ToString(), format);
            }
            catch (Exception e) {
                //catch exceptions and push to user (me, so i can debug)
                System.Windows.MessageBox.Show(e.ToString());
            }
        }
        /// <summary>
        /// Returns true if the hashes match. Returns false if hashes are different.
        /// </summary>
        /// <returns></returns>
        public bool CheckSameImage() {
            //get byte array of image and hash it
            this._newImgByteArray = ImageToByteArray(this._tempImage);
            this._newHash = HashImageByteArray(this._newImgByteArray);
            //if hashes match, then return true
            if(this._oldHash != null && this._newHash.SequenceEqual(this._oldHash)) {
                //check if oldhash is null. it might be the first time an image is captured so it is 100% new.
                return true;
            }
            //otherwise, save the image and save the newHash as the oldHash. return false because it is not a duplicate
            else {
                this._oldHash = this._newHash.Clone() as byte[];
                return false;
            }
        }

        public static byte[] ImageToByteArray(Object obj) {
            //Binary formatter object for serializing the memory stream to a byte array
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream()) {
                //serialize the object in the memory stream
                bf.Serialize(ms, obj);
                return ms.ToArray(); //return array of the serialized binary
            }
        }
        #region //private class for WinScreenshot to work. Nothing else needs this.
        /// <summary>
        /// Helper class. Handles GDI (graphics device interface) functions.
        /// </summary>
        private class GDI32 {
            public const int SRCCOPY = 0x00CC0020; //BitBlt dwRop parameter
            [DllImport("gdi32.dll")]
            public static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest,
                int nWidth, int nHeight, IntPtr hObjectSource,
                int nXSrc, int nYSrc, int dwRop);

            [DllImport("gdi32.dll")]
            public static extern IntPtr CreateCompatibleBitmap(IntPtr hDC, int nWidth,
                int nHeight);

            [DllImport("gdi32.dll")]
            public static extern IntPtr CreateCompatibleDC(IntPtr hDC);

            [DllImport("gdi32.dll")]
            public static extern bool DeleteDC(IntPtr hDC);

            [DllImport("gdi32.dll")]
            public static extern bool DeleteObject(IntPtr hObject);

            [DllImport("gdi32.dll")]
            public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
        }
        #endregion

        #region //private class for WinSCreenshot to work. Nothing else needs this.
        /// <summary>
        /// Importing User32 stuff for getting window details
        /// </summary>
        private class User32
        {
            [DllImport("user32.dll")]
            public static extern IntPtr GetDesktopWindow();
            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowDC(IntPtr hWnd);
            [DllImport("user32.dll")]
            public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDC);
            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowRect(IntPtr hWnd, ref RECT rect);
        }
        #endregion

        #region //i had to make this struct (copy from SO) because the one that microsoft provides breaks something fierce
        public struct RECT {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;

            public RECT(int left, int top, int right, int bottom) {
                this.Left = left;
                this.Top = top;
                this.Right = right;
                this.Bottom = bottom;
            }

            public int Width {
                get { return this.Right - this.Left; }
                set { this.Right = value + this.Left; }
            }

            public int Height {
                get { return this.Bottom - this.Top; }
                set { this.Bottom = value + this.Top; }
            }

            public System.Drawing.Point Position {
                get { return new System.Drawing.Point(this.Left, this.Top); }
            }

            public System.Drawing.Size Size {
                get { return new System.Drawing.Size(this.Width, this.Height); }
            }
        }
        #endregion
    }
    #endregion

    #region //a class for holding all the ebook data
    public class EBookData {
        private string _title;
        private string _author;
        private int _pageNumber;
        //the user directory is technically the folder that other folders will be saved in. 1 new book = 1 new folder.
        private string _userDirectory;
        //some safeguards before I actually start saving stuff
        private bool _directorySafe;
        private bool _nextPageSafe;
        public EBookData() {
            this._title = "";
            this._author = "";
            this._pageNumber = 0;
            this._userDirectory = "";
            //these have to be true before we can save the file
            this._directorySafe = false; //updated in private void checkuserdirectory
            this._nextPageSafe = false;  //updated in checkincrementpage
        }
        #region //getters and setters
        /// <summary>
        /// getter/setter for title field
        /// </summary>
        public string Title {
            get { return this._title; }
            set { this._title = value; }
        }
        /// <summary>
        /// getter/setter for author field
        /// </summary>
        public string Author {
            get { return this._author; }
            set { this._author = value; }
        }
        /// <summary>
        ///getter/setter for starting page field
        /// </summary>
        public string Page {
            get { return this._pageNumber.ToString("D8"); }
            set { this.CheckPageNumber(value); }
        }
        /// <summary>
        /// getter/setter for user directory
        /// </summary>
        public string UserDirectory {
            get { return this._userDirectory; }
            set { this.CheckUserDirectory(value); }
        }
        /// <summary>
        /// Keep checking if the directory and folder are safe to save.
        /// </summary>
        public bool SafeToSave {
            get {
                //make sure the directory is there and
                this.CheckUserDirectory(this._userDirectory);
                this.CheckIncrementPage();
                return (this._directorySafe && this._nextPageSafe);
            }
        }
        /// <summary>
        /// Gets full directory path.
        /// </summary>
        public string FullDirectoryPath {
            get { return this.UserDirectory + "\\" + this.Title + "_" + this.Author + "_page_" + this.Page; }
        }
        #endregion

        #region //helper functions
        public void IncrementPage() {
            this._pageNumber++;
        }

        /// <summary>
        /// Hiding the logic so I can make the setter code simpler.
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        private void CheckUserDirectory(string dir) {
            //if true, set the directory and set safety check to true
            if(Directory.Exists(dir)) {
                this._directorySafe = true;
                this._userDirectory = dir;
            }
            //if false, set directory to nothing and set safety check to false. also let the user know
            else {
                this._directorySafe = false;
                this._userDirectory = "";
                System.Windows.MessageBox.Show("The chosen directory doesn't exist somehow. Please try again or user a different directory.");
            }
        }
        /// <summary>
        /// condensing the setter into one line
        /// </summary>
        /// <param name="num"></param>
        private void CheckPageNumber(string num) {
            //try to parse as int
            if (int.TryParse(num, out this._pageNumber))
                return;
            else {
                System.Windows.MessageBox.Show("Couldn't parse as integer. Detaulting to 0.");
                this._pageNumber = 0;
            }
        }
        /// <summary>
        /// checks page increment. sets _nextPageSafe to false if something goes horribly wrong. otherwise,
        /// it will set it to true and allow the book to write another page image.
        /// </summary>
        /// <returns></returns>
        private void CheckIncrementPage() {
            int temp = this._pageNumber;
            try {
                temp = checked(this._pageNumber + 1);
                this._nextPageSafe = true;
            }
            catch {
                System.Windows.MessageBox.Show("Integer overflow on pages detected. Stopping ebook capture.");
                this._nextPageSafe = false;
            }
        }
        #endregion
    }
    #endregion
}
